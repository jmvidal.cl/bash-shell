#!/bin/bash

FILE_CONF='/etc/mysql/my.cnf'
USER_DB='root'
PASSWD_DB='xx2010'
BACKUP_DIR='/root/paso/full_backup'

mysqladmin \
--defaults-file=$FILE_CONF \
--user=$USER_DB \
--password=$PASSWD_DB \
shutdown

mysqlbackup \
--defaults-file=$FILE_CONF \
--backup-dir=$BACKUP_DIR \
apply-log

mysqlbackup \
--defaults-file=$FILE_CONF \
--backup-dir=$BACKUP_DIR \
copy-back

chown -R mysql:mysql /var/lib/mysql

/etc/init.d/mysql restart

