#!/bin/bash

#******************
# nombre: killProcessList.sh 
# creado: jmvidal
# version: 1.1
# fecha: 20130505
# descripcion: consulta por procesos pegados y mata el de mayor tiempo
# modificacion: 20130506 - jmvidal - se agrega notificacion de kill por mail
#********************************************************************************

MYSQL_USUARIO='root'
MYSQL_CLAVE='xx2010'
MYSQL_SERVER=localhost

CORREO_DESTINO='jmvidal.cl@gmail.com'

HORA_ACTUAL=$(date +"%H")
HORA_MAXIMA=19

if [ $HORA_ACTUAL -ge $HORA_MAXIMA ]
then
	PROCESOS_MINIMOS=4
else
	PROCESOS_MINIMOS=8
fi

echo $HORA_ACTUAL
echo $HORA_MAXIMA

TIEMPO_MINIMO=500
PROCESSLIST='./processlist.txt'
PROCESSLOG='./processlist.log'

echo "$(date) Inicio proceso..." >> $PROCESSLOG

echo "$(date) Consultando procesos..." >> $PROCESSLOG
mysql -u $MYSQL_USUARIO -p$MYSQL_CLAVE -h $MYSQL_SERVER --execute="show processlist;" > $PROCESSLIST

PROCESOS=$(cat $PROCESSLIST | grep 'Locked' | wc -l)

echo "$(date) Validando resultados..." >> $PROCESSLOG

if [ $PROCESOS -ge $PROCESOS_MINIMOS ]
then
    echo "$(date) Maximo de procesos bloqueados permitidos..." >> $PROCESSLOG
#    TIEMPOS=$(cat $PROCESSLIST | grep 'Locked' | awk '{print $12}' | sort -n | tail -1)
    TIEMPOS=$(cat $PROCESSLIST | grep 'Locked' | awk '{print $6}' | sort -n | tail -1)

    echo "$(date) Procesos bloqueados $TIEMPOS: " >> $PROCESSLOG
    echo "$(date) Tiempo blqueado $TIEMPO_MINIMO: " >> $PROCESSLOG

    if [ $TIEMPOS -ge $TIEMPO_MINIMO ]
    then
	echo "$(date) Tiempos de procesos bloqueados minimos superado..." >> $PROCESSLOG
#        PID=$(cat $PROCESSLIST | grep -v 'NULL' | awk '{print $12 " " $2}' | sort -n | tail -1 | awk '{print $2}')
        PID=$(cat $PROCESSLIST | grep -v 'NULL' | awk '{print $6 " " $1}' | sort -n | tail -1 | awk '{print $2}')
	LINEA=$(grep '$PID' $PROCESSLIST)
        ARCHIVO_MENSAJE="$(date) Se requiere realizar kill al siguiente proceso\n $LINEA"
	echo "mutt -s \"$(date) Kill Process $PID \" -- $CORREO_DESTINO < $ARCHIVO_MENSAJE" >> $PROCESSLOG
	mutt -s "$(date) Kill Process $PID " -- $CORREO_DESTINO < $ARCHIVO_MENSAJE

	echo "$(date) Matando proceso $PID" >> $PROCESSLOG
        mysql -u $MYSQL_USUARIO -p$MYSQL_CLAVE -h $MYSQL_SERVER --execute="kill $PID;"
    fi
fi

echo "$(date) Fin procesos..." >> $PROCESSLOG

