#!/bin/bash

#******************
# nombre: showProcessList.sh 
# creado: jmvidal
# version: 1.0
# fecha: 20130509
# descripcion: consulta por procesos en db y los reporta por mail
#********************************************************************************

MYSQL_USUARIO='root'
MYSQL_CLAVE='xx2010'
MYSQL_SERVER=localhost

CORREO_DESTINO='jmvidal.cl@gmail.com'
PROCESSLIST='/home/jmvidal/desarrollo/processlist'

ARCHIVO_HORA='/home/jmvidal/desarrollo/hora.txt'

HOST=$(hostname)
if [ $HOST = "jmvidal" ]
then 
	DEBUG=1
else 
	DEBUG=0
fi

if [ -f "$ARCHIVO_HORA" ]
then
   HORA_ANTERIOR=$(cat $ARCHIVO_HORA)
else
   echo $(date +"%H") > $ARCHIVO_HORA
   HORA_ANTERIOR=$(cat $ARCHIVO_HORA)
fi

HORA_ACTUAL=$(date +"%H")

if [ $HORA_ACTUAL -eq $HORA_ANTERIOR ]
then
	echo "$(date) Listado de procesos " >> $PROCESSLIST
	ENVIO=0
else
	cat $PROCESSLIST > $PROCESSLIST".txt"
	echo $(date +"%H") > $ARCHIVO_HORA
	echo "$(date) Listado de procesos " > $PROCESSLIST
	ENVIO=1
fi
	
mysql -u $MYSQL_USUARIO -p$MYSQL_CLAVE -h $MYSQL_SERVER --execute="show processlist;" >> $PROCESSLIST
cat $PROCESSLIST >> $PROCESSLIST".txt"

if [ $HOST != "jmvidal" ] && [ $ENVIO -eq 1 ]
then
        echo "enviando correo..."
	mutt -s "$(date) Listado de procesos " -a $PROCESSLIST".txt" -- $CORREO_DESTINO < $PROCESSLIST".txt"
fi

if [ $DEBUG = 1 ]
then
	cat $PROCESSLIST
fi

# FIN PROCESO

