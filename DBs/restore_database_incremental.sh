#!/bin/bash

FILE_CONF='/etc/mysql/my.cnf'
USER_DB='root'
PASSWD_DB='xx2010'
BACKUP_DIR='/root/paso/full_backup'
INCREMENTAL_DIR='/root/paso/incr_0002'

mysqladmin \
--defaults-file=$FILE_CONF \
--user=$USER_DB \
--password=$PASSWD_DB \
shutdown

mysqlbackup \
--backup-dir=$BACKUP_DIR \
--incremental-backup-dir=$INCREMENTAL_DIR \
apply-incremental-backup

chown -R mysql:mysql /var/lib/mysql

/etc/init.d/mysql restart

