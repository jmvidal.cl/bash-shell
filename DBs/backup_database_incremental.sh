#!/bin/bash

FILE_CONF='/etc/mysql/my.cnf'
USER_DB='root'
PASSWD_DB='xx2010'
DATABASES='asterisk'
BASE_DIR='dir:/root/paso/full_backup'

cd /root/paso

ls -1d incr_* > dir_back.tmp

CORRELATIVO=$(wc -l dir_back.tmp | awk '{print $1}')
CORRELATIVO=$(($CORRELATIVO+1))
CORRELATIVO=$(printf "%04d\n" $CORRELATIVO) 

BACKUP_DIR='/root/paso/incr_'$CORRELATIVO

echo $BACKUP_DIR

mysqlbackup \
--password=$PASSWD_DB \
--databases=$DATABASES \
--incremental \
--incremental-backup-dir=$BACKUP_DIR \
--incremental-base=$BASE_DIR \
backup


# borrando temporales
rm -rf dir_back.tmp
