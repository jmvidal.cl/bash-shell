#!/bin/bash

echo 1 > /proc/sys/net/ipv4/ip_forward

iptables -t nat --flush
iptables --flush
iptables -t mangle --flush

iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables -A FORWARD -i eth0 -j ACCEPT
