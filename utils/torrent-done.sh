#!/bin/bash

LOG_FILE="/home/pi/transmission.log"
TIMESTAMP=$(date +'%F %T')
MESSAGE="$TR_TIME_LOCALTIME ==> $TR_TORRENT_NAME"

DIR_SUB="\"$TR_TORRENT_DIR/$TR_TORRENT_NAME\""
MESSAGE="$TR_TORRENT_ID: [$TR_TORRENT_NAME] $TR_TORRENT_DIR "

echo $MESSAGE >> $LOG_FILE
#echo $TR_TIME_LOCALTIME >> $LOG_FILE
 
#export TELEGRAM_CONFIG_DIR=/home/pi/.telegram-cli

TO=jmvidal
EXEC=/usr/bin/telegram-cli
#PARAM=" -vvv -W /etc/telegram/tg-server.pub -k /etc/telegram/server.pub -U root -c /etc/telegram-cli/telegram-cli/config  -e "
PARAM=" -W -e "
#PARAM=" -vvv -W tg.pub -k /home/pi/tg/server.pub -U root -c /etc/telegram-cli/telegram-cli/config  -e "
#PARAM=" -vvv -k /home/pi/tg/server.pub -U root -W -e "

#(echo "safe_quit") | $EXEC $PARAM "msg $TO $MESSAGE !!!"
$EXEC $PARAM "msg $TO $MESSAGE !!!"

# subtitulo
#echo $DIR_SUB
#periscope -l es $DIR_SUB
filebot --output srt --lang "es" -get-subtitles $DIR_SUB
