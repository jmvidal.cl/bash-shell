#!/bin/bash

#********************
# jmvidal
# 20170715
#*****************************************

LOG_FILE="/home/pi/mensajeria-telegram.log"
TIMESTAMP=$(date +'%F %T')
MESSAGE="$TR_TIME_LOCALTIME ==> $TR_TORRENT_NAME"

#export TELEGRAM_CONFIG_DIR=/home/pi/.telegram-cli
TO=jmvidal
EXEC=/usr/bin/telegram-cli
#PARAM=" -vvv -W /etc/telegram/tg-server.pub -k /etc/telegram/server.pub -U root -c /etc/telegram-cli/telegram-cli/config  -e "
PARAM=" -W -e "
#PARAM=" -vvv -W tg.pub -k /home/pi/tg/server.pub -U root -c /etc/telegram-cli/telegram-cli/config  -e "
#PARAM=" -vvv -k /home/pi/tg/server.pub -U root -W -e "

# -- conf DB --
DB_HOST="localhost"
DB_NAME="desafio"
DB_USER="root"
DB_PASSWD="xx2010"

MOVIMIENTOS="movimientos.csv"

SQL="select b.celular, b.apellido_paterno, b.apellido_materno, b.nombres, c.archivo, d.descripcion_mensaje
     from movimientos as a
     inner join clientes as b on a.rut = b.rut
     inner join archivos as c on a.folio_archivo = c.folio_archivo
     inner join mensajes as d on a.codigo_mensaje = d.codigo_mensaje"

mysql -h $DB_HOST -u $DB_USER -p$DB_PASSWD $DB_NAME --execute "$SQL" > $MOVIMIENTOS 

CONT=0
while read -r movimiento
do
    CONT=$((CONT+1))

    if [ $CONT -gt 1 ] 
    then

    NUMERO=$(echo $movimiento | awk '{print $1}')
    MENSAJE=$(echo $movimiento | awk '{for(i=2;i<=NF;++i)print $i}')
    MENSAJE=$(echo $MENSAJE |  tr -d "\n") 
    echo $NUMERO
    echo $MENSAJE

    $EXEC $PARAM "msg $TO '$MENSAJE' !!!"
    echo "$EXEC $PARAM \"msg $TO $MENSAJE !!!\""

    fi
done < "$MOVIMIENTOS"


