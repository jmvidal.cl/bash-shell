#!/bin/bash

#*********************
# archivo: upload_adt.sh
# creado: 20130420
# por: jmvidal
# descripcion: envia gestiones via correo a ADT
# midificacion: 20130507 - jmvidal - se arregla contador de lineas
#		- se modifica fecha inicio, debe ser desde el 01 de cada mes
#****************************************************************************

# CONFIGURACION CONEXION
SERVER='localhost'
USERDB='root'
PASSWD='xx2010'
DATABASE='sgl'

ARCH_FECHA=$(date +"%Y%m%d")
ARCH_SALIDA="./gestiones_adt_"$ARCH_FECHA

FECHA_INICIO=$(date +"%Y-%m-01 00:00:00")
FECHA_TERMINO=$(date +"%Y-%m-%d %T")

CORREO_DESTINO='dario.cabeza@cobranzaslegaliza.cl'
CORREO_CC='jmvidal.cl@gmail.com'

# QUERY A EJECUTAR
SQL="CALL sp_upload_report_adt('$FECHA_INICIO', '$FECHA_TERMINO');"
mysql --host=$SERVER --password=$PASSWD --user=$USERDB --database=$DATABASE --execute="$SQL" > $ARCH_SALIDA".tmp"

sed 's/\t/\;/g' $ARCH_SALIDA".tmp" > $ARCH_SALIDA".txt"
rar a $ARCH_SALIDA".rar" $ARCH_SALIDA".txt" 2>/dev/null

REGISTROS=$(wc -l $ARCH_SALIDA".txt" | awk '{print $1}')
REGISTROS=$((REGISTROS-1))

MENSAJE="Estimados\n\nAdjunto Informe de gestiones ADT, el detalle es el siguiente:\n\nRegistros: $REGISTROS \nFecha Inicio: $FECHA_INICIO \nFechar Termino: $FECHA_TERMINO \n\nSaludos Cordiales"
ARCH_MENSAJE="mensaje.txt"

# ENVIO DE CORREO
echo $MENSAJE > $ARCH_MENSAJE
echo " " >> $ARCH_MENSAJE
#cat $ARCH_SALIDA >> $ARCH_MENSAJE
mutt -s "Informes gestiones ADT " -a $ARCH_SALIDA".rar" -c $CORREO_CC -- $CORREO_DESTINO < $ARCH_MENSAJE

#LIMPIEZA
rm -rf $ARCH_SALIDA".*"

# FIN SCRIPT ENVIO CORREO

