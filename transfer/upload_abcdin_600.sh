#!/bin/bash

#*********************
# archivo: upload_abcdin_600.sh
# creado: 20130420
# por: jmvidal
# descripcion: envia gestiones ABCDIN 600 via ftp
# modificacion: 20130507 - jmvidal - se arregla contador de lineas
#******************************************************************

# CONFIGURACION CONEXION
SERVER='localhost'
USERDB='root'
PASSWD='xx2010'
DATABASE='sgl'

FTP_SERVER='ftp.din.cl'
FTP_USER='usrlegaliza'
FTP_PASSWD='*2812legal.,'
FTP_RUTA='.'

ARCH_FECHA=$(date +"%y%m%d")
ARCH_SALIDA="./b600_sa121_"$ARCH_FECHA

FECHA_INICIO=$(date +"%Y-%m-%d 00:00:00")
FECHA_TERMINO=$(date +"%Y-%m-%d %T")

CORREO_DESTINO='dario.cabeza@cobranzaslegaliza.cl'
CORREO_CC='jmvidal.cl@gmail.com'

# QUERY A EJECUTAR
SQL="CALL sp_upload_report_abcdin_600('$FECHA_INICIO', '$FECHA_TERMINO');"
mysql --host=$SERVER --password=$PASSWD --user=$USERDB --database=$DATABASE --execute="$SQL" > $ARCH_SALIDA".tmp"

sed 's/\t//g' $ARCH_SALIDA".tmp" > $ARCH_SALIDA".txt"
rar a $ARCH_SALIDA".rar" $ARCH_SALIDA".txt" 2>/dev/null

# ENVIO POR FTP
ncftpput -u $FTP_USER -p $FTP_PASSWD $FTP_SERVER $FTP_RUTA $ARCH_SALIDA".rar"

REGISTROS=$(wc -l $ARCH_SALIDA".txt" | awk '{print $1}')
REGISTROS=$((REGISTROS-1))
#
MENSAJE="Estimados\n\nAdjunto Informe de gestiones 600 ABCDIN, el detalle es el siguiente:\n\nRegistros: $REGISTROS \nFecha Inicio: $FECHA_INICIO \nFechar Termino: $FECHA_TERMINO \n\nSaludos Cordiales"
ARCH_MENSAJE="mensaje.txt"

# ENVIO DE CORREO
echo $MENSAJE > $ARCH_MENSAJE
echo " " >> $ARCH_MENSAJE
#cat $ARCH_SALIDA >> $ARCH_MENSAJE
mutt -s "Informes gestiones 600 ABCDIN " -a $ARCH_SALIDA".rar" -c $CORREO_CC -- $CORREO_DESTINO < $ARCH_MENSAJE

rm -rf $ARCH_SALIDA".*"

# FIN SCRIPT ENVIO CORREO
