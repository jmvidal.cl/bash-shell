#!/bin/bash

DIR_BASE="/home/pi/Videos/BonJovi"
FILES="listado.txt"
FILTRO="*.mkv .mp4 .avi"

locate $FILTRO | sort --reverse  > $FILES

CONT=0
TOTAL=$(wc -l $FILES | awk '{print $1}')
while read -r file 
do

    CONT=$((CONT+1))

    echo "1 => $CONT / $TOTAL => $file"
    echo "2 => $CONT / $TOTAL => omxplayer -o hdmi \"$file\""

    omxplayer -o hdmi --blank "$file"

    echo "..........."

done < "$FILES"

